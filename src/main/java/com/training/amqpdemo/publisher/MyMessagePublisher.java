package com.training.amqpdemo.publisher;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyMessagePublisher {

	public static void main(String[] args) {
		
		AbstractApplicationContext context = 
				new ClassPathXmlApplicationContext("Beans.xml");
		
		AmqpTemplate template = context.getBean(RabbitTemplate.class);
		
		for (int i = 1; i <= 5; i++) {
			String message = "Hello All! msg #"+i;
			System.out.print("Sending Message #"+i);
			template.convertAndSend(message);
			System.out.println(" ......Sent.OK");
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		context.close();
	}

}
